﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX;
using SharpDX.Toolkit;
using SharpDX.Toolkit.Input;
using Windows.UI.Input;

namespace SentientWarsCuriosity.Engine
{
    using Gameplay;

    public class SphericalCamera : Camera
    {
        private float rMin = 5.0f;
        private float rMax = 50.0f;
        protected float prevTime = 0.0f;
        public readonly float phiMax = 0.80f;
        public readonly float phiMin = -0.80f;


        public SphericalCamera(SentientWarsCuriosity game, Matrix initView, Matrix initProjection, float rMin, float rMax)
            : base(game, initView, initProjection)
        {
            setRMin(rMin);
            setRMax(rMax);
        }
        public SphericalCamera(SentientWarsCuriosity game, float rMin, float rMax)
            : base(game)
        {
            setRMin(rMin);
            setRMax(rMax);
        }
        public override void Update(GameTime gameTime)
        { 
            float currTime = (float)gameTime.TotalGameTime.TotalSeconds;
            float deltaT = currTime - this.prevTime;
            this.prevTime = currTime;
            this.camProjection = (float) Math.PI / 4.0f;
            setProjection(Matrix.PerspectiveFovLH(this.camProjection, (float)game.GraphicsDevice.BackBuffer.Width / game.GraphicsDevice.BackBuffer.Height, 0.1f,
                    100.0f));
            //Determine transformation speeds
            float yRotationAngularSpeed = 0.0f;
            float vertRotationAngularSpeed = 0.0f;
            float rTranslationSpeed = 0.0f;
            //Interpret keyboard state
            if (game.keyboardState.IsKeyDown(Keys.Left))
            {
                yRotationAngularSpeed += 3.0f;
            }
            if (game.keyboardState.IsKeyDown(Keys.Right))
            {
                yRotationAngularSpeed += -3.0f;
            }
            if (game.keyboardState.IsKeyDown(Keys.Z))
            {
                rTranslationSpeed += -10.0f;
            }
            if (game.keyboardState.IsKeyDown(Keys.X))
            {
                rTranslationSpeed += 10.0f;
            }
            if (game.keyboardState.IsKeyDown(Keys.Up))
            {
                vertRotationAngularSpeed += +1.0f;
            }
            if (game.keyboardState.IsKeyDown(Keys.Down))
            {
                vertRotationAngularSpeed += -1.0f;
            }
            //Interpret accelerometer


            //Convert to absolute angles and distances
            float yRotationAngle = yRotationAngularSpeed * deltaT;
            float vertRotationAngle = vertRotationAngularSpeed * deltaT;
            float rTranslation = rTranslationSpeed * deltaT;
            Matrix currentTransform = getCameraTransform();
            //Zoom in or out, constant speed
            //TODO: exponentialise this
            currentTransform = this.translationR(currentTransform, rTranslation);
            //Rotate vertically
            currentTransform = this.rotationVertical(currentTransform, vertRotationAngle);
            //Rotate about Y
            currentTransform = currentTransform * Matrix.RotationY(yRotationAngle);
            setCameraTransform(currentTransform);
        }

        public void update(City tempCity)
        {
            this.camProjection = (float)Math.PI / 4.0f;
            setProjection(Matrix.PerspectiveFovLH(this.camProjection, (float)game.GraphicsDevice.BackBuffer.Width / game.GraphicsDevice.BackBuffer.Height, 0.1f,
                    100.0f));
            Matrix currentTransform = getCameraTransform();
            
        }

        protected Matrix rotationVertical(Matrix oldTransform, float angle)
        {
            float currentPhi = Vector3.Dot(Vector3.Normalize(oldTransform.Forward), Vector3.UnitY);
            //Bound phi to prevent camera inversion
            float newAngle = currentPhi + angle;
            newAngle = Math.Max(newAngle, this.phiMin);
            newAngle = Math.Min(newAngle, this.phiMax);
            angle = newAngle - currentPhi;
            Matrix newTransform = oldTransform * Matrix.RotationAxis(oldTransform.Right, angle);
            return newTransform;
        }
        private Matrix translationR(Matrix oldTransform, float distance)
        {
            float oldR = oldTransform.TranslationVector.Length();
            float newR = oldR + distance;
            newR = clipR(newR);
            float dR = newR - oldR;
            Matrix newTransform = oldTransform * Matrix.Translation(dR * oldTransform.Forward);
            return newTransform;

        }
        public override void scaleCamera(float scale)
        {
            float currentR = this.getCameraTransform().TranslationVector.Length();
            float newR = currentR / scale;
            newR = clipR(newR);
            float newScale = newR / currentR;
            Matrix scaledTransform = this.getCameraTransform() * Matrix.Scaling(newScale);
            this.setCameraTransform(scaledTransform);
        }
        private float clipR(float r)
        {
            r = Math.Max(r, this.rMin);
            r = Math.Min(r, this.rMax);
            return r;
        }
        public void setRMin(float rMin)
        {
            this.rMin = rMin;
        }
        public void setRMax(float rMax)
        {
            this.rMax = rMax;
        }
        public float clipPhi(float yTranslation)
        {
            double temp = yTranslation;
            temp = Math.Min(this.phiMax, temp);
            temp = Math.Max(this.phiMin, temp);
            return (float)temp;
        }

        public override void OnManipulationUpdated(GestureRecognizer sender, ManipulationUpdatedEventArgs args)
        {
            this.scaleCamera(args.Delta.Scale);
            Matrix temp = this.getCameraTransform();
            float yRotationAngle = (float)args.Delta.Translation.X / 600;
            float vertRotationAngle = clipPhi((float)args.Delta.Translation.Y / 600);
            temp = temp * Matrix.RotationY(yRotationAngle);
            temp = this.rotationVertical(temp, vertRotationAngle);
            setCameraTransform(temp);
        }
        
    }
}
