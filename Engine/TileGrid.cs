﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX;
using SharpDX.Toolkit;
using System.Diagnostics;

namespace SentientWarsCuriosity.Engine
{
    using SharpDX.Toolkit.Graphics;

    using Engine.Object;
    using Engine.Object.Shape;
    using Engine.Lighting;
    using Gameplay;

    public class TileGrid : GameEntity
    {
        private Tile[,,] tile;
        private int tilePerRow;

        private static readonly int FRONT = 0;
        private static readonly int BACK = 1;
        private static readonly int LEFT = 2;
        private static readonly int RIGHT = 3;
        private static readonly int TOP = 4;
        private static readonly int BOTTOM = 5;

        private static readonly int NFACES = 6;

        Planet planet;

        // creates a tile grid for QuadSphere Planet.
        public TileGrid(SentientWarsCuriosity game, Vector3 startpos, Planet setplanet)
        {
            planet = setplanet;

            QuadSphere qs = planet.getQuadSphere();

            Queue<Tuple<int, int>> coords = qs.getCoordMap();
            int nCoords = coords.Count;
            this.tilePerRow = (int)Math.Sqrt(nCoords);
           
            tile = new Tile[6, tilePerRow, tilePerRow];

            generateGrid();

            vertices = Buffer.Vertex.New(game.GraphicsDevice, generateVertices());

            effect = game.Content.Load<Effect>("Phong");

            inputLayout = VertexInputLayout.FromBuffer(0, vertices);
            this.game = game;
        }

        public override void Update(GameTime gameTime)
        {
            // Rotate the cube.
            var time = (float)gameTime.TotalGameTime.TotalSeconds;
            world = Matrix.Translation(pos);
            worldInverseTranspose = Matrix.Transpose(Matrix.Invert(world));
        }

        private void generateGrid()
        {
            
            QuadSphere qs = planet.getQuadSphere();

            Queue<Tuple<int, int>> coords = qs.getCoordMap();
            int nCoords = coords.Count;
            int nCoordsl = (int) Math.Sqrt(nCoords);
            
            Queue<Tuple<Vector3, Vector3, Vector3, Vector3>> qsqueue = qs.qsqueue;      
            
            for (int i = 0; i < NFACES; i++)
            {
                // Tuple<Vector3, Vector3, Vector3, Vector3>[,] face = faces.Dequeue();

                for (int j = 0; j < nCoords; j++)
                {
                    Tuple<int, int> coord = coords.Dequeue();
                    coords.Enqueue(coord);

                    // Wierd Coord flipping going on
                    int x = coord.Item2;
                    int y = coord.Item1;

                    tile[i, x, y] = new Tile(qsqueue.Dequeue(), planet, i);
                }
            }

        }
        
        private VertexPositionNormalColor[] generateVertices()
        {
            VertexPositionNormalColor[] vpnc = new VertexPositionNormalColor[2 * 3];
            

            Vector3 item1 = tile[BOTTOM, 15, 15].getTileCoord().Item1;
            Vector3 item2 = tile[BOTTOM, 15, 15].getTileCoord().Item2;
            Vector3 item3 = tile[BOTTOM, 15, 15].getTileCoord().Item3;
            Vector3 item4 = tile[BOTTOM, 15, 15].getTileCoord().Item4;

            Vector3 normal = Vector3.Cross(item2 - item1, item3 - item1);
            normal.Normalize();
            //Debug.WriteLine(normal);
            vpnc[5] = new VertexPositionNormalColor(item1, normal, Color.White);
            vpnc[4] = new VertexPositionNormalColor(item2, normal, Color.White);
            vpnc[3] = new VertexPositionNormalColor(item3, normal, Color.White);
            vpnc[2] = new VertexPositionNormalColor(item1, normal, Color.Black);
            vpnc[1] = new VertexPositionNormalColor(item3, normal, Color.Black);
            vpnc[0] = new VertexPositionNormalColor(item4, normal, Color.Black);

            return vpnc;
            
        }

        // generates nCities for the given player on the TileGrid.
        public List<City> generateCities(int nCities, Player player, SentientWarsCuriosity game)
        {
            Random rand = new Random(System.DateTime.UtcNow.Second);
            List<City> city = new List<City>();

            int cities = 0;
            while (cities != nCities)
            {
                int face = rand.Next() % NFACES;
                int x = rand.Next() % tilePerRow;
                int y = rand.Next() % tilePerRow;

                if (tile[face, x, y].getTileType() == Tile.TileType.LAND && !tile[face, x, y].getContainCity())
                {
                    tile[face, x, y].setContainCity(true);
                    city.Add(new City(game, player, tile[face, x, y], rand.Next()));
                    cities++;
                }
            }
            return city;
        }
          
    }

    public struct Tile
    {
        public enum TileType
        {
            LAND,
            INHABITABLE,
            LIQUID,
        };

        private Planet owner;
        private Vector3 tileLoc;
        private float height;
        TileType tileType;
        private bool containCity;
        public int face;
        private Tuple<Vector3, Vector3, Vector3, Vector3> tileCoord;

        public Tile(Tuple<Vector3, Vector3, Vector3, Vector3> square, Planet p, int face)
        {
            float x = (square.Item1.X + square.Item2.X + square.Item3.X + square.Item4.X) / 4;
            float y = (square.Item1.Y + square.Item2.Y + square.Item3.Y + square.Item4.Y) / 4;
            float z = (square.Item1.Z + square.Item2.Z + square.Item3.Z + square.Item4.Z) / 4;

            this.owner = p;
            this.tileLoc = new Vector3(x, y, z);
            this.height = tileLoc.Length() - p.size;
            this.tileCoord = square;
            this.tileType = determineType(height, p);
            this.containCity = false;
            this.face = face;
        }

        public Planet getPlanet()
        {
            return this.owner;
        }

        void changeTile(TileType type)
        {
            this.tileType = type;
        }

        public Tuple<Vector3, Vector3, Vector3, Vector3> getTileCoord()
        {
            return this.tileCoord;
        }

        public Vector3 getTileLoc()
        {
            return this.tileLoc;
        }

        public TileType getTileType()
        {
            return this.tileType;
        }

        static TileType determineType(float h, Planet p)
        {
            if (h * 10 < 0.0f)
                return TileType.LIQUID;
            else if (h * 10 < p.getQuadSphere().front.habitableLand)
                return TileType.LAND;
            else 
                return TileType.INHABITABLE;
        }

        public bool getContainCity()
        {
            return this.containCity;
        }

        public void setContainCity(bool gotCity)
        {
            this.containCity = gotCity;
        }

        public Vector3 getSurfaceNormal()
        {
            Vector3 cross1 = Vector3.Cross(Vector3.Subtract(this.getTileCoord().Item2, this.getTileCoord().Item1), 
                Vector3.Subtract(this.getTileCoord().Item3,this.getTileCoord().Item1));
            Vector3 cross2 = Vector3.Cross(Vector3.Subtract(this.getTileCoord().Item4, this.getTileCoord().Item1),
                Vector3.Subtract(this.getTileCoord().Item4, this.getTileCoord().Item1));
            return (cross1 + cross2) / 2;
        }

    }

}
