﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SentientWarsCuriosity.Engine
{
    using SharpDX.Toolkit;

    public class GameDateTime
    {
        public static readonly string[] monthName = new string[] {
            "Newton",
            "Galileo",
            "Columbus",
            "Einstein",
            "Copernicus",
            "Huygens",
            "Hawking",
            "Sagan",
        };

        public static readonly string yearSymbol = "AE";
        public static readonly int startYear = 670;
        public static readonly int endYear = 1000;
        public static readonly int maxMonthInYear = 8;
        public static readonly int maxDayInMonth = 50;
        public static readonly int startTimeOfDay = 0;
        public static readonly int endTimeOfDay = 30;

        public string year { get; set; }
        public string month { get; set; }
        public string day { get; set; }
        public string time { get; set; }

        public GameDateTime()
        {
            this.year = null;
            this.month = null;
            this.day = null;
            this.time = null;
        }

        public void update(GameTime gameTime)
        {
            double gTime = gameTime.TotalGameTime.TotalSeconds;
            double remainder;

            year = ((int)(gTime / (GameDateTime.monthName.Length * GameDateTime.maxDayInMonth * GameDateTime.endTimeOfDay))).ToString() + yearSymbol;
            remainder = gTime % (GameDateTime.monthName.Length * GameDateTime.maxDayInMonth * GameDateTime.endTimeOfDay);
            month = monthName[(int)(remainder / (GameDateTime.endTimeOfDay * GameDateTime.maxDayInMonth))];
            remainder = remainder % (remainder * GameDateTime.maxDayInMonth);
            day = ((int)(remainder / GameDateTime.endTimeOfDay)).ToString();
            remainder = remainder % GameDateTime.endTimeOfDay;
            if (remainder > 15)
                time = ((int)remainder).ToString() + "Yin";
            else
                time = ((int)remainder).ToString() + "Yang";
        }
    }
}
