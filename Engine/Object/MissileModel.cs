﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX.Toolkit.Input;
using SharpDX;
using SharpDX.Toolkit;
using System.Diagnostics;
namespace SentientWarsCuriosity.Engine.Object
{
    using SharpDX.Toolkit.Graphics;
    using Windows.UI.Input;

    public abstract class MissileModel
    {
        private Model model;
        private Matrix world;
        private Matrix view;
        private Matrix projection;
        public Vector3 pos;

        public SentientWarsCuriosity game;

        public MissileModel(SentientWarsCuriosity game)
        {
            model = game.Content.Load<Model>("comMissile");
            BasicEffect.EnableDefaultLighting(model, true);
            this.game = game;

        }

        public abstract void Update(GameTime gameTime);

        public void Draw(GameTime gameTime)
        {
            // Draw the model
            model.Draw(game.GraphicsDevice, world, view, projection);
        }
        protected void setWorld(Matrix world)
        {
            this.world = world;
        }
        protected void setView(Matrix view)
        {
            this.view = view;
        }
        protected void setProjection(Matrix projection)
        {
            this.projection = projection;
        }
    }
    
}
