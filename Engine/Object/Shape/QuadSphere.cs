﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX;
using SharpDX.Toolkit;
using System.IO;
using System.Diagnostics;

namespace SentientWarsCuriosity.Engine.Object.Shape
{
    using SharpDX.Toolkit.Graphics;
    using Engine.Lighting;

    public class QuadSphere
    {
        // raw_icosqueue 
        public Queue<Tuple<Vector3, Vector3, Vector3, Vector3>> qsqueue = new Queue<Tuple<Vector3, Vector3, Vector3, Vector3>>();
        public int tessellationlvl = 0;

        // 6 planes to keep track of
        public Landscape front;
        public Landscape back;
        public Landscape left;
        public Landscape right;
        public Landscape top;
        public Landscape bottom;

        private float size;
        private PlanetType type;

        private Queue<Tuple<int, int>> coords;

        public QuadSphere(int tess, float setsize, PlanetType settype)
        {
            size = setsize;
            type = settype;
            qsqueue.Enqueue(Tuple.Create(new Vector3(-1, -1, -1), new Vector3(-1, 1, -1), new Vector3(1, 1, -1), new Vector3(1, -1, -1))); // Front
            qsqueue.Enqueue(Tuple.Create(new Vector3(1, -1, 1), new Vector3(1, 1, 1), new Vector3(-1, 1, 1), new Vector3(-1, -1, 1))); // Back
            qsqueue.Enqueue(Tuple.Create(new Vector3(-1, -1, 1), new Vector3(-1, 1, 1), new Vector3(-1, 1, -1), new Vector3(-1, -1, -1))); // Left
            qsqueue.Enqueue(Tuple.Create(new Vector3(1, -1, -1), new Vector3(1, 1, -1), new Vector3(1, 1, 1), new Vector3(1, -1, 1))); // Right
            qsqueue.Enqueue(Tuple.Create(new Vector3(-1, 1, -1), new Vector3(-1, 1, 1), new Vector3(1, 1, 1), new Vector3(1, 1, -1))); // Top
            qsqueue.Enqueue(Tuple.Create(new Vector3(1, -1, -1), new Vector3(1, -1, 1), new Vector3(-1, -1, 1), new Vector3(-1, -1, -1))); // Bottom

            tessellate(tess);

            Boolean acceptable = false;

            while (!acceptable)
            {

                // To temp hold reverses
                float[] reverse_side;

                // Front Side
                front = (new Landscape(tess));

                front.generate();

                // Left Side
                left = (new Landscape(tess));

                left.setSide(Side.RIGHT, front.getSide(Side.LEFT));

                left.generate();

                // Back Side
                back = (new Landscape(tess));

                back.setSide(Side.RIGHT, left.getSide(Side.LEFT));

                back.generate();

                // Right Side
                right = (new Landscape(tess));

                right.setSide(Side.RIGHT, back.getSide(Side.LEFT));
                right.setSide(Side.LEFT, front.getSide(Side.RIGHT));

                right.generate();

                // Top Side
                top = (new Landscape(tess));

                reverse_side = back.getSide(Side.TOP);
                Array.Reverse(reverse_side);
                top.setSide(Side.TOP, reverse_side);

                top.setSide(Side.RIGHT, right.getSide(Side.TOP));

                top.setSide(Side.BOTTOM, front.getSide(Side.TOP));

                reverse_side = left.getSide(Side.TOP);
                Array.Reverse(reverse_side);
                top.setSide(Side.LEFT, reverse_side);

                top.generate();

                // Bottom Side
                bottom = (new Landscape(tess));

                reverse_side = front.getSide(Side.BOTTOM);
                Array.Reverse(reverse_side);
                bottom.setSide(Side.BOTTOM, reverse_side);

                reverse_side = left.getSide(Side.BOTTOM);
                Array.Reverse(reverse_side);
                bottom.setSide(Side.RIGHT, reverse_side);

                bottom.setSide(Side.TOP, back.getSide(Side.BOTTOM));
                bottom.setSide(Side.LEFT, right.getSide(Side.BOTTOM));

                bottom.generate();

                if (type.Equals(PlanetType.EARTH))
                {
                    float percentWater = (front.percentWater() + back.percentWater() + left.percentWater() + right.percentWater() + top.percentWater() + bottom.percentWater()) / 6;
                    if (percentWater > 0.25 && percentWater < 0.75)
                    {
                        acceptable = true;
                    }
                }
                else
                {
                    // Not earth, we just let it go
                    acceptable = true;
                }
            }



            // Generate Coords
            int tsLvl = tessellationlvl;

            coords = new Queue<Tuple<int, int>>();

            int side = (int)Math.Pow(2, tsLvl) + 1;
            int mid = side;

            coords.Enqueue(Tuple.Create(0, 0));

            for (int i = 0; i < tsLvl; i++)
            {
                mid = mid / 2;
                int npoints = coords.Count;
                for (int j = 0; j < npoints; j++)
                {
                    Tuple<int, int> coord = coords.Dequeue();
                    int x = coord.Item1;
                    int y = coord.Item2;
                    coords.Enqueue(Tuple.Create(x, y));
                    coords.Enqueue(Tuple.Create(x + mid, y));
                    coords.Enqueue(Tuple.Create(x + mid, y + mid));
                    coords.Enqueue(Tuple.Create(x, y + mid));
                }
            }
            
        }

        public VertexPositionNormalColor[] generateVertices()
        {
            Queue<Landscape> terrains = new Queue<Landscape>();
            Landscape land = front;
            float[,] terrain = land.heightMap;
            terrains.Enqueue(front);
            terrains.Enqueue(back);
            terrains.Enqueue(left);
            terrains.Enqueue(right);
            terrains.Enqueue(top);
            terrains.Enqueue(bottom);

            Queue<Tuple<int, int>> coords = getCoordMap();

            Queue<Tuple<Vector3, Vector3, Vector3, Vector3>> newqsqueue = new Queue<Tuple<Vector3, Vector3, Vector3, Vector3>>();

            // Number of Vertices depend on level of tesselation
            VertexPositionNormalColor[] qsVPC = new VertexPositionNormalColor[qsqueue.Count * 2 * 3];

            int nperface = qsqueue.Count / 6;

            // TODO
            for (int i = 0; i < qsqueue.Count; i++)
            {

                if (i % nperface == 0)
                {
                    land = terrains.Dequeue();
                    terrain = land.heightMap;
                }

                Tuple<int, int> coord = coords.Dequeue();
                coords.Enqueue(coord);

                // Wierd Coord flipping going on
                int x = coord.Item2;
                int y = coord.Item1;

                float height1 = terrain[x, y];                
                Vector3 item1mod = Vector3.Multiply(Vector3.Normalize(qsqueue.ElementAt(i).Item1), size * 0.01f * land.seaCheck(height1));
                Vector3 item1 = Vector3.Multiply(qsqueue.ElementAt(i).Item1, size);
                Vector3 item1h = Vector3.Add(item1, item1mod);
                float height2 = terrain[x, y + 1];
                Vector3 item2mod = Vector3.Multiply(Vector3.Normalize(qsqueue.ElementAt(i).Item2), size * 0.01f * land.seaCheck(height2));
                Vector3 item2 = Vector3.Multiply(qsqueue.ElementAt(i).Item2, size);
                Vector3 item2h = Vector3.Add(item2, item2mod);
                float height3 = terrain[x + 1, y + 1];
                Vector3 item3mod = Vector3.Multiply(Vector3.Normalize(qsqueue.ElementAt(i).Item3), size * 0.01f * land.seaCheck(height3));
                Vector3 item3 = Vector3.Multiply(qsqueue.ElementAt(i).Item3, size);
                Vector3 item3h = Vector3.Add(item3, item3mod);
                float height4 = terrain[x + 1, y];
                Vector3 item4mod = Vector3.Multiply(Vector3.Normalize(qsqueue.ElementAt(i).Item4), size * 0.01f * land.seaCheck(height4));
                Vector3 item4 = Vector3.Multiply(qsqueue.ElementAt(i).Item4, size);
                Vector3 item4h = Vector3.Add(item4, item4mod);

                qsVPC[i * 6 + 0] = new VertexPositionNormalColor(item1h, item1h, land.heightcol(height1, type));
                qsVPC[i * 6 + 1] = new VertexPositionNormalColor(item2h, item2h, land.heightcol(height2, type));
                qsVPC[i * 6 + 2] = new VertexPositionNormalColor(item3h, item3h, land.heightcol(height3, type));
                qsVPC[i * 6 + 3] = new VertexPositionNormalColor(item1h, item1h, land.heightcol(height1, type));
                qsVPC[i * 6 + 4] = new VertexPositionNormalColor(item3h, item3h, land.heightcol(height3, type));
                qsVPC[i * 6 + 5] = new VertexPositionNormalColor(item4h, item4h, land.heightcol(height4, type));

                // Minute raise for tilegrid
                float raise = 1.001f;
                Tuple<Vector3, Vector3, Vector3, Vector3> items = Tuple.Create(
                    Vector3.Multiply(item1h, raise),
                    Vector3.Multiply(item2h, raise),
                    Vector3.Multiply(item3h, raise),
                    Vector3.Multiply(item4h, raise));
                newqsqueue.Enqueue(items);

            }
            qsqueue = newqsqueue;
            return qsVPC;
        }

        private void tessellate(int tesselationLevel)
        {
            Tuple<Vector3, Vector3, Vector3, Vector3> face;
            Vector3 v1;
            Vector3 v2;
            Vector3 v3;
            Vector3 v4;
            Vector3 v5;
            Vector3 v6;
            Vector3 v7;
            Vector3 v8;
            Vector3 v9;

            tessellationlvl += tesselationLevel;

            for (int i = 0; i < tesselationLevel; i++)
            {
                int nfaces = qsqueue.Count;

                for (int j = 0; j < nfaces; j++)
                {
                    face = qsqueue.Dequeue();
                    v1 = face.Item1;
                    v1 = Vector3.Multiply(v1, (1.0f / (float)v1.Length()));
                    v2 = face.Item2;
                    v2 = Vector3.Multiply(v2, (1.0f / (float)v2.Length()));
                    v3 = face.Item3;
                    v3 = Vector3.Multiply(v3, (1.0f / (float)v3.Length()));
                    v4 = face.Item4;
                    v4 = Vector3.Multiply(v4, (1.0f / (float)v4.Length()));
                    v5 = v1 + v2;
                    v5 = Vector3.Multiply(v5, (1.0f / (float)v5.Length()));
                    v6 = v2 + v3;
                    v6 = Vector3.Multiply(v6, (1.0f / (float)v6.Length()));
                    v7 = v3 + v4;
                    v7 = Vector3.Multiply(v7, (1.0f / (float)v7.Length()));
                    v8 = v4 + v1;
                    v8 = Vector3.Multiply(v8, (1.0f / (float)v8.Length()));
                    v9 = v1 + v2 + v3 + v4;
                    v9 = Vector3.Multiply(v9, (1.0f / (float)v9.Length()));

                    // Create 4 new faces per face
                    qsqueue.Enqueue(Tuple.Create(v1, v5, v9, v8));
                    qsqueue.Enqueue(Tuple.Create(v5, v2, v6, v9));
                    qsqueue.Enqueue(Tuple.Create(v9, v6, v3, v7));
                    qsqueue.Enqueue(Tuple.Create(v8, v9, v7, v4));
                }
            }
        }

        public Queue<Tuple<int, int>> getCoordMap()
        {
            return coords;
        }
    }
}