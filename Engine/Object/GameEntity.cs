﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX;
using SharpDX.Toolkit;

namespace SentientWarsCuriosity.Engine.Object
{
    using SharpDX.Toolkit.Graphics;

    using Engine.Lighting;
    using Windows.UI.Input;

    abstract public class GameEntity
    {
        public Effect effect;
        public VertexInputLayout inputLayout;
		public Buffer<VertexPositionNormalColor> vertices;
        public SentientWarsCuriosity game;
        public Vector3 pos;
        public Vector3 lightsrc;
        public Vector3 lightsrcpos;
        public SpriteBatch word;

        public Matrix world;
        public Matrix worldInverseTranspose;

        public abstract void Update(GameTime gametime);

        public void Draw(GameTime gameTime)
        {

            effect.Parameters["World"].SetValue(world);
            effect.Parameters["Projection"].SetValue(game.camera.getProjection());            
            effect.Parameters["View"].SetValue(game.camera.getView());
            effect.Parameters["cameraPos"].SetValue(game.camera.getCameraPos());
            effect.Parameters["worldInvTrp"].SetValue(worldInverseTranspose);
            effect.Parameters["lightPntPos"].SetValue(lightsrc);

            // Setup the vertices
            game.GraphicsDevice.SetVertexBuffer(vertices);
            game.GraphicsDevice.SetVertexInputLayout(inputLayout);
            

            // Apply the basic effect technique and draw the rotating cube
            effect.CurrentTechnique.Passes[0].Apply();
            game.GraphicsDevice.Draw(PrimitiveType.TriangleList, vertices.ElementCount);
        }

        public Vector3 getWorldPosition()
        {
            Vector3 translation = this.world.TranslationVector;
            return new Vector3(translation.X, translation.Y, translation.Z);
        }
        public Matrix getWorldTransform()
        {
            return this.world;
        }

        // These virtual voids allow any object that extends GameObject to respond to tapped and manipulation events
        public virtual void Tapped(GestureRecognizer sender, TappedEventArgs args)
        {

        }

        public virtual void OnManipulationStarted(GestureRecognizer sender, ManipulationStartedEventArgs args)
        {

        }

        public virtual void OnManipulationUpdated(GestureRecognizer sender, ManipulationUpdatedEventArgs args)
        {

        }

        public virtual void OnManipulationCompleted(GestureRecognizer sender, ManipulationCompletedEventArgs args)
        {

        }
    }
}
