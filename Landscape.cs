﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX;
using SharpDX.Toolkit;
using System.Diagnostics;

namespace SentientWarsCuriosity
{
    class Landscape
    {

        private float[,] height_map;

        public Landscape(int level)
        {
            int length = 513;

            // Random Seed Corners
            Random rand = new Random();
            // Initial Corners
            float ll = rand.NextFloat(-0.05f * length, 0.05f * length);
            float ul = rand.NextFloat(-0.05f * length, 0.05f * length);
            float ur = rand.NextFloat(-0.05f * length, 0.05f * length);
            float lr = rand.NextFloat(-0.05f * length, 0.05f * length);

            // Assign corners to heightmap
            height_map = new float[length, length];
            height_map[0, 0] = ll;
            height_map[0, length - 1] = ul;
            height_map[length - 1, length - 1] = ur;
            height_map[length - 1, 0] = lr;

            // Use midpoint_displacement algorithm to complete heightmap
            //midpoint_displacement(0, 0,length);
            midpoint_displacement(0, 0, length - 1, length - 1, 0.35f);
        }

        private void midpoint_displacement(int ini_x, int ini_y, int end_x, int end_y, float rough_factor)
        {

            int mid_x = (ini_x + end_x) / 2;
            int mid_y = (ini_y + end_y) / 2;

            // Create random factor for mid-point
            Random rand = new Random();
            float rand_num = rand.NextFloat(-1f, 1f) * (end_x - ini_x) * rough_factor;

            // Calculate Mid-Point
            height_map[mid_x, mid_y] = (height_map[ini_x, ini_y] + height_map[ini_x, end_y] + height_map[end_x, end_y] + height_map[end_x, ini_y]) / 4 + rand_num;

            // Calculate Sides

            height_map[ini_x, mid_y] = (height_map[ini_x, ini_y] + height_map[ini_x, end_y]) / 2;
            height_map[mid_x, end_y] = (height_map[ini_x, end_y] + height_map[end_x, end_y]) / 2;
            height_map[end_x, mid_y] = (height_map[end_x, end_y] + height_map[end_x, ini_y]) / 2;
            height_map[mid_x, ini_y] = (height_map[end_x, ini_y] + height_map[ini_x, ini_y]) / 2;

            // When distance is 2, it means that 3x3 squares have been encountered (ie. final interation)
            if ((end_x - ini_x) < 3)
            {
                return;
            }
            else
            {
                // Roughness factor of 65% every iteration
                rough_factor *= 0.65f;
                midpoint_displacement(ini_x, ini_y, mid_x, mid_y, rough_factor);
                midpoint_displacement(ini_x, mid_y, mid_x, end_y, rough_factor);
                midpoint_displacement(mid_x, mid_y, end_x, end_y, rough_factor);
                midpoint_displacement(mid_x, ini_y, end_x, mid_y, rough_factor);
            }
        }


    }
}
